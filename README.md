# Instruction

1. Fork this repository and clone it to your desktop or laptop.
2. **If you're using GitHub Desktop**, import this repository to your eclipse's workspace.
3. Add the `twitterlib` and `twitter4j-core-4.0.4` jar to the project's build path
4. Read the problem `2110215_Lab6_2016.pdf` posted on CourseVille thoroughly.
5. Implement the program as specified in the problem.
6. Export your finished project into a runnable jar file, with source codes, named `Lab6_2016_{ID}.jar` where ID is your Student ID.
  * For example, `Lab6_2016_5770257521.jar`.
  * Make sure to export it into your project root directory.
7. Commit and Push it to your GitHub repository.
8. Send a Pull Request to us with Title: `[STUDENT-ID] Submission`.

# Deadline
Friday 9th December 2016
