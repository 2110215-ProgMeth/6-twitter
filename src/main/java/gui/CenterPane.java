package gui;

import javafx.scene.layout.VBox;

public class CenterPane extends VBox{	
	
	public CenterPane(double spacing){
		super(spacing);
		
		this.setPrefSize(600, 400);
		this.setMaxHeight(400);
	}
	
	public void addTweetListCell(TweetListCell cell){
		this.getChildren().add(cell);
	}
}
