package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{

	public static Main instance;
	
	private TopPane topPane;
	private CenterPane centerPane;
	
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Main.instance = this;
		
		BorderPane root = new BorderPane();
		
		this.topPane = new TopPane();
		this.centerPane = new CenterPane(10);
		
		root.setTop(topPane);
		ScrollPane sp = new ScrollPane(centerPane);
		root.setCenter(sp);
		
		Scene scene = new Scene(root);
		
		primaryStage.setTitle("Twitter");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.sizeToScene();
		primaryStage.show();

	}

	public TopPane getTopPane() {
		return this.topPane;
	}

	public CenterPane getCenterPane() {
		return this.centerPane;
	}
	
	
}
