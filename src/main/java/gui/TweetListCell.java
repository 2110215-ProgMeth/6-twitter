package gui;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import twitterlib.Tweet;

public class TweetListCell extends BorderPane {

	private ImageView avatarImageView;
	private Label tweetLabel;
	
	public TweetListCell(Tweet tweet) {
		
		super();
		
		this.avatarImageView = new ImageView(tweet.getProfileImage());
		this.tweetLabel = new Label("@" + tweet.getScreenName() + " : " + tweet.getText());
		this.tweetLabel.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		this.tweetLabel.setPadding(new Insets(0, 10 , 0, 10));
		this.tweetLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		this.setLeft(this.avatarImageView);
		this.setCenter(this.tweetLabel);
	}
	
}
