package gui;

import java.util.ArrayList;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import twitter4j.Status;
import twitterlib.Tweet;
import twitterlib.TwitterConnector;
import twitterlib.TwitterQueryMissingException;

public class TopPane extends BorderPane {
	
	private Label searchLabel;
	private TextField searchField;
	private Button searchButton;
	
	public TopPane() {
		
		super();
		initializeGUI();
		searchButton.setOnAction(e->{
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try{
						onAction();
					}
					catch (TwitterQueryMissingException e) {
						// TODO: handle exception
						Platform.runLater(()->{
							Alert a=new Alert(AlertType.ERROR);
							a.setTitle("Error: Query missing");
							a.setHeaderText(null);
							a.setContentText("Please fill the keyword");
							a.show();
						});
					}
				}
			}).start();
		});
	}
	private void onAction() throws TwitterQueryMissingException{
		String searchText=searchField.getText();
		if(searchText.matches("^\\s*$")){
			throw new TwitterQueryMissingException();
		}
		disableSearchButton();
		Platform.runLater(()->{
			Main.instance.getCenterPane().getChildren().clear();
		});
		
		TwitterConnector tc;
		try {
			tc = new TwitterConnector();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		ArrayList<Thread> tl=new ArrayList<>();
		for(Status s:tc.searchAndProcess(searchText)){
			Thread t=new Thread(()->{
				Tweet tw=new Tweet(s);
				TweetListCell tlc=new TweetListCell(tw);
				Platform.runLater(()->{
					Main.instance.getCenterPane().getChildren().add(tlc);
				});
			});
			t.start();
			tl.add(t);
		}
		for(Thread t:tl){
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		enableSearchButton();
	}
	public void disableSearchButton(){
		this.searchButton.setDisable(true);
	}
	
	public void enableSearchButton(){
		this.searchButton.setDisable(false);
	}
	
	private void initializeGUI(){
		this.searchLabel = new Label("Query string : ");
		this.searchLabel.setPadding(new Insets(2.5,10,0,10));
		
		this.searchField = new TextField();
		
		this.searchButton = new Button("Search");
		
		this.setLeft(this.searchLabel);
		this.setCenter(this.searchField);
		this.setRight(this.searchButton);
	}

}
